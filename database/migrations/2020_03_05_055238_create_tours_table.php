<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('user_id');
			$table->string('field1');
			$table->string('field2')->nullable($value = true);
			$table->string('field3')->nullable($value = true);
			$table->string('field4')->nullable($value = true);
			$table->string('field5')->nullable($value = true);
			$table->string('field6')->nullable($value = true);
			$table->string('field7')->nullable($value = true);
			$table->string('field8')->nullable($value = true);
			$table->string('field9')->nullable($value = true);
			$table->string('field10')->nullable($value = true);
			$table->string('field11')->nullable($value = true);
			$table->string('field12')->nullable($value = true);
			$table->string('field13')->nullable($value = true);
			$table->string('field14')->nullable($value = true);
			$table->string('field15')->nullable($value = true);
			$table->string('field16')->nullable($value = true);
			$table->string('field17')->nullable($value = true);
			$table->string('field18')->nullable($value = true);
			$table->string('field19')->nullable($value = true);
			$table->string('field20')->nullable($value = true);
			$table->string('field21')->nullable($value = true);
			$table->string('field22')->nullable($value = true);
			$table->string('field23')->nullable($value = true);
			$table->string('field24')->nullable($value = true);
			$table->string('field25')->nullable($value = true);
			$table->string('field26')->nullable($value = true);
			$table->string('field27')->nullable($value = true);
			$table->string('field28')->nullable($value = true);
			$table->string('field29')->nullable($value = true);
			$table->string('field30')->nullable($value = true);
			$table->string('field31')->nullable($value = true);
			$table->string('field32')->nullable($value = true);
			$table->string('field33')->nullable($value = true);
			$table->string('field34')->nullable($value = true);
			$table->string('field35')->nullable($value = true);
			$table->string('field36')->nullable($value = true);
			$table->string('field37')->nullable($value = true);
			$table->string('field38')->nullable($value = true);
			$table->string('field39')->nullable($value = true);
			$table->string('field40')->nullable($value = true);
			$table->decimal('cost_price', 8, 2)->nullable($value = true);
			$table->decimal('cost', 8, 2)->nullable($value = true);
			$table->decimal('incoming', 8, 2)->nullable($value = true);
			$table->decimal('outgoing', 8, 2)->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
