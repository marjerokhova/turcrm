cost = $("#tours-cost-price");
price = $("#tours-cost");
comission = $("#tours-comission");
incoming = $("#tours-incoming");
outgoing = $("#tours-outgoing");
debt_info = $(".tours-debt-info");
debt1 = $(".tours-debt1");
debt2 = $(".tours-debt2");
prize = $(".tours-prize");
inv_btn = $(".invoice_button");
contr_btn = $(".contract_button");
form_tour = $("#save_tour_form");
inv_btn.on('click', function(e) {
	e.preventDefault(); // 
	$.ajax({
		type: form_tour.attr('method'),
		url: form_tour.attr('action'),
		data: form_tour.serialize(),
		success: function (data) {
			window.open(inv_btn[0].getAttribute("href"), '_blank');
		},
		error: function(data) {
			console.log('error');
		}
	});
});

contr_btn.on('click', function(e) {
	e.preventDefault(); // 
	$.ajax({
		type: form_tour.attr('method'),
		url: form_tour.attr('action'),
		data: form_tour.serialize(),
		success: function (data) {
		  window.open(contr_btn[0].getAttribute("href"), '_blank');
		},
		error: function(data) {
			console.log('error');
		}
	});
});

function comissionCalculate()
{
	if (price.val() != "" && cost.val() != "")
		comission.val(price.val() - cost.val()); 
	debtCalculate();
}
comissionCalculate();
cost.on(
	"keyup",
	comissionCalculate
	);
price.on(
	"keyup",
	comissionCalculate
	);
	
function debtCalculate()
{
	// incoming - приход, т.е. сумма полученная от клиента 
	// outgoing - расход, т.е. сумма, оплаченная турагенством туроператору
	// debt1 - долг турагенства перед туроператором, себестоимость тура - расход
	// debt2 - долг клиента перед турагенством, цена тура - приход
	// prize - премия менеджера, выдается при отсутсвии долгов перед ТО и ТА, равна 20% от комиссии
	debt1.val(cost.val() - outgoing.val());
	debt2.val(price.val() - incoming.val());
	if (debt1.val()==0 && debt2.val()==0)
		prize.val(comission.val()*0.2);
	else
		prize.val("Премия выдается при отсутсвии задолженности!");
	if (cost.val() != "" && price.val() != "" && incoming.val() != "" && outgoing.val() != "")
		debt_info.removeClass("d-none");
	else
		debt_info.addClass("d-none");
}
debtCalculate();
incoming.on(
	"keyup",
	debtCalculate
	);
	
outgoing.on(
	"keyup",
	debtCalculate
	);