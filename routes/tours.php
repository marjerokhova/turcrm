<?php

use Illuminate\Support\Facades\Route;
use App\Tours;
use Illuminate\Http\Request;

Route::get('/', 'HomeController@index');
Auth::routes();
/**
* Вывести приходный кассовый ордер
*/
Route::get('/{tour}/invoice', 'InvoiceController@download');
/**
* Вывести договор
*/
Route::get('/{tour}/contract', 'ContractController@download');
/**
* Добавить новый тур
*/
Route::post('/', 'ToursController@create');
/**
* Удалить тур
*/
Route::delete('/{tour}', 'ToursController@delete');
/**
* Вывести страницу с информацией о существующем туре
*/	
Route::get('/{tour}', 'ToursController@get');
/**
* Сохранить изменения в туре
*/
Route::post('/{tour}', 'ToursController@save'); 