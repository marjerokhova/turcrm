<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Tours;

class ToursController extends Controller
{
    public function create(Request $request) {
		$tour = new Tours;
		$tour->user_id = Auth::user()->id;
		$tour->field1 = $request->field1;
		$tour->field2 = $request->field2;
		$tour->field3 = $request->field3;
		$tour->field4 = $request->field4;
		$tour->field5 = $request->field5;
		$tour->field6 = $request->field6;
		$tour->field7 = $request->field7;
		$tour->field8 = $request->field8;
		$tour->field9 = $request->field9;
		$tour->field10 = $request->field10;
		$tour->field11 = $request->field11;
		$tour->field12 = $request->field12;
		$tour->field13 = $request->field13;
		$tour->field14 = $request->field14;
		$tour->field15 = $request->field15;
		$tour->field16 = $request->field16;
		$tour->field17 = $request->field17;
		$tour->field18 = $request->field18;
		$tour->field19 = $request->field19;
		$tour->field20 = $request->field20;
		$tour->field20 = $request->field20;
		$tour->field21 = $request->field21;
		$tour->field22 = $request->field22;
		$tour->field23 = $request->field23;
		$tour->field24 = $request->field24;
		$tour->field25 = $request->field25;
		$tour->field26 = $request->field26;
		$tour->field27 = $request->field27;
		$tour->field28 = $request->field28;
		$tour->field29 = $request->field29;
		$tour->field30 = $request->field30;
		$tour->field31 = $request->field31;
		$tour->field32 = $request->field32;
		$tour->field33 = $request->field33;
		$tour->field34 = $request->field34;
		$tour->field35 = $request->field35;
		$tour->field36 = $request->field36;
		$tour->field37 = $request->field37;
		$tour->field38 = $request->field38;
		$tour->field39 = $request->field39;
		$tour->field40 = $request->field40;
		$tour->cost_price = $request->cost_price;
		$tour->cost = $request->cost;
		$tour->incoming = $request->incoming;
		$tour->outgoing = $request->outgoing;
		$tour->save();

		return view('tour', [
			'tour' => $tour
		]);
	}
	
	public function delete(Tours $tour) {
		$tour->delete();

		return redirect('/tours');
	}
	
	public function get(Tours $tour) {
		return view('tour', [
			'tour' => $tour
		]);
	}
	
	public function save(Request $request, Tours $tour) {
		$tour->field1 = $request->field1;
		$tour->field2 = $request->field2;
		$tour->field3 = $request->field3;
		$tour->field4 = $request->field4;
		$tour->field5 = $request->field5;
		$tour->field6 = $request->field6;
		$tour->field7 = $request->field7;
		$tour->field8 = $request->field8;
		$tour->field9 = $request->field9;
		$tour->field10 = $request->field10;
		$tour->field11 = $request->field11;
		$tour->field12 = $request->field12;
		$tour->field13 = $request->field13;
		$tour->field14 = $request->field14;
		$tour->field15 = $request->field15;
		$tour->field16 = $request->field16;
		$tour->field17 = $request->field17;
		$tour->field18 = $request->field18;
		$tour->field19 = $request->field19;
		$tour->field20 = $request->field20;
		$tour->field20 = $request->field20;
		$tour->field21 = $request->field21;
		$tour->field22 = $request->field22;
		$tour->field23 = $request->field23;
		$tour->field24 = $request->field24;
		$tour->field25 = $request->field25;
		$tour->field26 = $request->field26;
		$tour->field27 = $request->field27;
		$tour->field28 = $request->field28;
		$tour->field29 = $request->field29;
		$tour->field30 = $request->field30;
		$tour->field31 = $request->field31;
		$tour->field32 = $request->field32;
		$tour->field33 = $request->field33;
		$tour->field34 = $request->field34;
		$tour->field35 = $request->field35;
		$tour->field36 = $request->field36;
		$tour->field37 = $request->field37;
		$tour->field38 = $request->field38;
		$tour->field39 = $request->field39;
		$tour->field40 = $request->field40;
		$tour->cost_price = $request->cost_price;
		$tour->cost = $request->cost;
		$tour->incoming = $request->incoming;
		$tour->outgoing = $request->outgoing;
		$tour->save();

		return view('tour', [
			'tour' => $tour
		]);
	}
}
