<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Tours;


class ContractController extends Controller
{
    public function download(Tours $tour)
    {
        $params["price"] = $tour->incoming;
		$params["name"] = $tour->field2;
		$params["purpose"] = "Оплата тура ".$tour->field1;
		$_monthsList = array(
		"1"=>"Января","2"=>"Февраля","3"=>"Марта",
		"4"=>"Апреля","5"=>"Мая", "6"=>"Июня",
		"7"=>"Июля","8"=>"Августа","9"=>"Сентября",
		"10"=>"Октября","11"=>"Ноября","12"=>"Декабря");

		$params["month"] = mb_strtolower($_monthsList[date("n")]);
		$params["date"] = date ("d");
		$params["num_doc"] = $tour->id;
		$params["name"] = $tour->field2;
		$params["organization"] = "Турфирма ЛяЛяЛя";
		$params["staff"] = Auth::user()->name."(прим. информация авторизованного пользователя)";
		$params["order"] = "Приказ №256 для ЛяЛяЛя";
		$params["reestr_num"] = "№2568974568921456";
		$params["dop_info"] = $tour->field3."(прим. Поле 3)";

		$view = \View::make('pdf.contract_form', compact('params'))->render();
		$html = mb_convert_encoding($view, 'HTML-ENTITIES', "UTF-8"); 
		$pdf = \App::make('dompdf.wrapper');
		$pdf->loadHTML($view); 
		return $pdf->stream('contract'); 
    }
}
