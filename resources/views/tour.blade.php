@extends('layouts.app')

@section('content')

<!-- Bootstrap шаблон... -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">		
			<div class="panel-body m-2">
				<!-- Отображение ошибок проверки ввода -->
				@include('common.errors')
				<div id="cur_tour">
					<div class="row bg-success d-none tours-debt-info m-2">
						<div class="col-sm">
							<div class="form-group">
								<label for="debt1" class="control-label">Долг перед туроператором</label>
								<div>
									<input type="number" name="debt1" class="tours-debt1 form-control" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label for="debt2" class="control-label">Долг перед турагентом</label>
								<div>
									<input type="number" name="debt2" class="tours-debt2 form-control" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label for="prize" class="control-label">Премия</label>
								<div>
									<input type="text" name="prize" class="tours-prize form-control" readonly>
								</div>
							</div>
						</div>
					</div>
					<a class="btn btn-secondary invoice_button" href="{{ url('tours/'.$tour->id.'/invoice') }}" target="_blank" role="button">Распечатать приходно кассовый ордер</a>
					<a class="btn btn-secondary contract_button" href="{{ url('tours/'.$tour->id.'/contract') }}" target="_blank" role="button">Распечатать договор</a>
					<!-- Форма с информацией о текущем туре -->
					<form id="save_tour_form" action="{{ url('tours/'.$tour->id) }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field1" class="control-label">Название тура</label>
									<div>
										<input type="text" name="field1" id="tours-field1" class="form-control" value="{{ $tour->field1 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field2" class="control-label">Имя клиента</label>
									<div>
										<input type="text" name="field2" id="tours-field2" class="form-control" value="{{ $tour->field2 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field3" class="control-label">Поле 3</label>
									<div>
										<input type="text" name="field3" id="tours-field3" class="form-control"  value="{{ $tour->field3 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field4" class="control-label">Поле 4</label>
									<div>
										<input type="text" name="field4" id="tours-field4" class="form-control" value="{{ $tour->field4 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field5" class="control-label">Поле 5</label>
									<div>
										<input type="text" name="field5" id="tours-field5" class="form-control" value="{{ $tour->field5 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field6" class="control-label">Поле 6</label>
									<div>
										<input type="text" name="field6" id="tours-field6" class="form-control"  value="{{ $tour->field6 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field7" class="control-label">Поле 7</label>
									<div>
										<input type="text" name="field7" id="tours-field7" class="form-control" value="{{ $tour->field7 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field8" class="control-label">Поле 8</label>
									<div>
										<input type="text" name="field8" id="tours-field8" class="form-control" value="{{ $tour->field8 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field9" class="control-label">Поле 9</label>
									<div>
										<input type="text" name="field9" id="tours-field9" class="form-control" value="{{ $tour->field9 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field10" class="control-label">Поле 10</label>
									<div>
										<input type="text" name="field10" id="tours-field10" class="form-control" value="{{ $tour->field10 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field11" class="control-label">Поле 11</label>
									<div>
										<input type="text" name="field11" id="tours-field11" class="form-control" value="{{ $tour->field11 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field12" class="control-label">Поле 12</label>
									<div>
										<input type="text" name="field12" id="tours-field12" class="form-control" value="{{ $tour->field12 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field13" class="control-label">Поле 13</label>
									<div>
										<input type="text" name="field13" id="tours-field13" class="form-control" value="{{ $tour->field13 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field14" class="control-label">Поле 14</label>
									<div>
										<input type="text" name="field14" id="tours-field14" class="form-control" value="{{ $tour->field14 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field15" class="control-label">Поле 15</label>
									<div>
										<input type="text" name="field15" id="tours-field15" class="form-control" value="{{ $tour->field15 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field16" class="control-label">Поле 16</label>
									<div>
										<input type="text" name="field16" id="tours-field16" class="form-control" value="{{ $tour->field16 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field17" class="control-label">Поле 17</label>
									<div>
										<input type="text" name="field17" id="tours-field17" class="form-control" value="{{ $tour->field17 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field18" class="control-label">Поле 18</label>
									<div>
										<input type="text" name="field18" id="tours-field18" class="form-control" value="{{ $tour->field18 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field19" class="control-label">Поле 19</label>
									<div>
										<input type="text" name="field19" id="tours-field19" class="form-control" value="{{ $tour->field19 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field20" class="control-label">Поле 20</label>
									<div>
										<input type="text" name="field20" id="tours-field20" class="form-control" value="{{ $tour->field20 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field21" class="control-label">Поле 21</label>
									<div>
										<input type="text" name="field21" id="tours-field21" class="form-control" value="{{ $tour->field21 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field22" class="control-label">Поле 22</label>
									<div>
										<input type="text" name="field22" id="tours-field22" class="form-control" value="{{ $tour->field22 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field23" class="control-label">Поле 23</label>
									<div>
										<input type="text" name="field23" id="tours-field23" class="form-control" value="{{ $tour->field23 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field24" class="control-label">Поле 24</label>
									<div>
										<input type="text" name="field24" id="tours-field24" class="form-control" value="{{ $tour->field24 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field25" class="control-label">Поле 25</label>
									<div>
										<input type="text" name="field25" id="tours-field25" class="form-control" value="{{ $tour->field25 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field26" class="control-label">Поле 26</label>
									<div>
										<input type="text" name="field26" id="tours-field26" class="form-control" value="{{ $tour->field26 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field27" class="control-label">Поле 27</label>
									<div>
										<input type="text" name="field27" id="tours-field27" class="form-control" value="{{ $tour->field27 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field28" class="control-label">Поле 28</label>
									<div>
										<input type="text" name="field28" id="tours-field28" class="form-control" value="{{ $tour->field28 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field29" class="control-label">Поле 29</label>
									<div>
										<input type="text" name="field29" id="tours-field29" class="form-control" value="{{ $tour->field29 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field30" class="control-label">Поле 30</label>
									<div>
										<input type="text" name="field30" id="tours-field30" class="form-control" value="{{ $tour->field30 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field31" class="control-label">Поле 31</label>
									<div>
										<input type="text" name="field31" id="tours-field31" class="form-control" value="{{ $tour->field31 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field32" class="control-label">Поле 32</label>
									<div>
										<input type="text" name="field32" id="tours-field32" class="form-control" value="{{ $tour->field32 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field33" class="control-label">Поле 33</label>
									<div>
										<input type="text" name="field33" id="tours-field33" class="form-control" value="{{ $tour->field33 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field34" class="control-label">Поле 34</label>
									<div>
										<input type="text" name="field34" id="tours-field34" class="form-control" value="{{ $tour->field34 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field35" class="control-label">Поле 35</label>
									<div>
										<input type="text" name="field35" id="tours-field35" class="form-control" value="{{ $tour->field35 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field36" class="control-label">Поле 36</label>
									<div>
										<input type="text" name="field36" id="tours-field36" class="form-control" value="{{ $tour->field36 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field37" class="control-label">Поле 37</label>
									<div>
										<input type="text" name="field37" id="tours-field37" class="form-control" value="{{ $tour->field37 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field38" class="control-label">Поле 38</label>
									<div>
										<input type="text" name="field38" id="tours-field38" class="form-control" value="{{ $tour->field38 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field39" class="control-label">Поле 39</label>
									<div>
										<input type="text" name="field39" id="tours-field39" class="form-control" value="{{ $tour->field39 }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field40" class="control-label">Поле 40</label>
									<div>
										<input type="text" name="field40" id="tours-field40" class="form-control" value="{{ $tour->field40 }}">
									</div>
								</div>
							</div>
						</div>
						<div class="row bg-info">
							<div class="col-sm">
								<div class="form-group">
									<label for="cost_price" class="control-label">Себестоимость</label>
									<div>
										<input type="number" name="cost_price" id="tours-cost-price" class="form-control" value="{{ $tour->cost_price }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="cost" class="control-label">Цена</label>
									<div>
										<input type="number" name="cost" id="tours-cost" class="form-control" value="{{ $tour->cost }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="comission" class="control-label">Комиссия</label>
									<div>
										<input type="number" name="comission" id="tours-comission" class="form-control" readonly>
									</div>
								</div>
							</div>
						</div>
						<div class="row bg-info">
							<div class="col-sm">
								<div class="form-group">
									<label for="incoming" class="control-label">Приход</label>
									<div>
										<input type="number" name="incoming" id="tours-incoming" class="form-control" value="{{ $tour->incoming }}">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="outgoing" class="control-label">Расход</label>
									<div>
										<input type="number" name="outgoing" id="tours-outgoing" class="form-control" value="{{ $tour->outgoing }}">
									</div>
								</div>
							</div>
						</div>
						<!-- Кнопка сохранения тура -->
						<div class="form-group">
							<div class="my-2">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus"></i> Сохранить
								</button>
								<a class="btn btn-secondary invoice_button" href="{{ url('tours/'.$tour->id.'/invoice') }}" target="_blank" role="button">Распечатать приходно кассовый ордер</a>
								<a class="btn btn-secondary contract_button" href="{{ url('tours/'.$tour->id.'/contract') }}" target="_blank" role="button">Распечатать договор</a>
							</div>
						</div>
					</form>
					<div class="row bg-success d-none tours-debt-info m-2">
						<div class="col-sm">
							<div class="form-group">
								<label for="debt1" class="control-label">Долг перед туроператором</label>
								<div>
									<input type="number" name="debt1" class="tours-debt1 form-control" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label for="debt2" class="control-label">Долг перед турагентом</label>
								<div>
									<input type="number" name="debt2" class="tours-debt2 form-control" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label for="prize" class="control-label">Премия</label>
								<div>
									<input type="text" name="prize" class="tours-prize form-control" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection