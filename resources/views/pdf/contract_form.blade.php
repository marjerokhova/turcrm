<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ДОГОВОР</title>
  <style>
    @font-face {
      font-family: "DejaVu Sans";
      font-style: normal;
      src: url("/fonts/dejavu-sans/DejaVuSans.ttf");
      /* IE9 Compat Modes */
      src: 
        local("DejaVu Sans"), 
        local("DejaVu Sans"), 
        url("/fonts/dejavu-sans/DejaVuSans.ttf") format("truetype");
    }
    body { 
      font-family: "DejaVu Sans";
    }
  </style>
 </head>
 <body style="font-size: 14px;">
 <h5 align="center">ДОГОВОР № {{ $params["num_doc"] }}/20 <br> о реализации туристского продукта</h5>
 <table width="100%">
 <tr>
	<td align="left" width="5%">
	</td>
	<td align="left" width="5%">
		город
	</td>
	<td align="left" width="5%" style="border-bottom:1 px solid black;">
		Вологда
	</td>
	<td align="left" width="45%">
	</td>
	<td align="right" width="5%" style="border-bottom:1 px solid black;">
		«{{ $params["date"] }}»
	</td>
	<td align="center" style="border-bottom:1 px solid black;">
		&nbsp;{{ $params["month"] }}
	</td>
	<td align="right" width="15%">
		2020 года
	</td>
 </tr>
 </table>
 <table width="100%">
 <tr>
	<td colspan=2>Турагент:</td>
 </tr>
 <tr>
	<td style="border-bottom:1 px solid black;" width="99%"> {{ $params["organization"] }} </td><td>,</td>
 </tr>
 <tr>
	<td  colspan=2 align="center" style="font-size:9px;">полное наименование юридического лица/ФИО индивидуального предпринимателя; указание на знак обслуживания - при наличии</td>
 </tr>
 </table>
 <table width="100%">
 <tr>
	<td>в лице</td><td style="border-bottom:1 px solid black;" width="90%">{{ $params["staff"] }}</td>
 </tr>
 <tr>
	<td colspan=2 align="center" style="font-size:9px;">Ф.И.О.- полностью, должность - при наличии</td>
 </tr>
 </table>
 <table width="100%">
 <tr>
	<td>действующего на основании</td><td style="border-bottom:1 px solid black;" width="68%">{{ $params["order"] }}</td>
 </tr>
 <tr>
	<td>&nbsp;</td><td align="center" style="font-size:9px;" width="68%">наименование и реквизиты документа - при наличии</td>
 </tr>
 </table>
 <table width="100%">
 <tr>
	<td colspan=2>от своего имени, по поручению и за счет <b>Туроператора</b>:</td>
 </tr>
 <tr>
	<td style="border-bottom:1 px solid black;" width="99%">{{ $params["reestr_num"] }}</td><td>,</td>
 </tr>
 <tr>
	<td colspan=2 align="center" style="font-size:9px;">полное наименование, реестровый номер Туроператора, а также реквизиты договора на реализацию туристского продукта,</td>
 </tr>
 <tr>
	<td colspan=2 align="center" style="font-size:9px;">заключенного между Туроператором и Турагентом</td>
 </tr>
 </table>
 <table width="100%">
 <tr>
	<td colspan=2>с одной стороны,</td>
 </tr>
 <tr>
	<td colspan=2>и, именуемый далее <b>Заказчик</b>:</td>
 </tr>
 <tr>
	<td style="border-bottom:1 px solid black;" width="99%">{{ $params["name"] }}</td><td>,</td>
 </tr>
 <tr>
	<td colspan=2 align="center" style="font-size:9px;">Ф.И.О. физического лица или индивидуального предпринимателя - полностью, либо полное наименование юридического лица</td>
 </tr>
 <tr>
	<td colspan=2>действующий на основании</td>
 </tr>
 <tr>
	<td style="border-bottom:1 px solid black;" width="99%">{{ $params["dop_info"] }}</td><td>,</td>
 </tr>
 <tr>
	<td colspan=2 align="center" style="font-size:9px;">указываются полномочия Заказчика на заключение настоящего договора в случае, если Заказчик действует, в том числе, в</td>
 </tr>
 <tr>
	<td colspan=2 align="center" style="font-size:9px;">интересах других лиц – туристов, и/или не является туристом</td>
 </tr>
 </table>
 <table width="100%">
 <tr>
	<td colspan=2>с другой стороны, вместе именуемые Стороны, заключили настоящий договор (далее - Договор) о
нижеследующем:
</td>
 </tr>
 </table>
<h5 align="center"> 1. ПРЕДМЕТ ДОГОВОРА </h5>
<ul style="list-style-type:none;">
	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1. В соответствии с Договором Туроператор обязуется обеспечить оказание Заказчику комплекса услуг по перевозке и размещению и иных услуг (далее - Туристский продукт), полный перечень и потребительские свойства которых указаны в заявке на бронирование (Приложение №1 к Договору), а Заказчик обязуется оплатить Туристский продукт.
	</li>
	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.2. Сведения о Заказчике и (или) туристе, в объеме, необходимом для исполнения Договора, указаны в заявке на бронирование. Информация о Туроператоре указана в Приложении №2 к Договору.
	</li>
	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3. Продвижение и реализация Туристского продукта осуществляются Турагентом на основании договора, заключенного между Туроператором и Турагентом. По настоящему договору Турагент является исполнителем и несет самостоятельную ответственность перед туристом и (или) Заказчиком за непредставление или представление недостоверной информации о Туристском продукте, за неисполнение или ненадлежащее исполнение обязательств по настоящему Договору.
	</li>
</ul>
<h5 align="center">2. ОБЩАЯ ЦЕНА ТУРИСТСКОГО ПРОДУКТА И ПОРЯДОК ОПЛАТЫ</h5>
<ul style="list-style-type:none;">
	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1. Общая цена Туристского продукта указана в заявке на бронирование.
	</li>
	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.2. Оплата осуществляется Заказчиком путем перечисления денежных средств на расчетный счет Турагента или вносится наличными в кассу Турагента в течение 1 (одного) рабочего дня, следующего за днем подписания Приложения №1 к настоящему Договору. Заказчик также вправе произвести оплату непосредственно Туроператору.
	</li>
	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3. Если Туроператор в течение 48 часов с момента поступления заявки на бронирование Туристского продукта не подтвердит возможность его бронирования статусом «доступно к оплате», Стороны будут считать, что невозможность исполнения обязательств возникла по обстоятельствам, за которые ни одна из Сторон не отвечает. В этом случае Турагент возвращает Заказчику все полученные от него денежные средства.
	</li>
</ul>
<h5 align="center">3. ВЗАИМОДЕЙСТВИЕ ТУРАГЕНТА, ТУРОПЕРАТОРА И ЗАКАЗЧИКА</h5>
<ul style="list-style-type:none;">
	<li><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.1. Обязательства по оплате Туристского продукта: </b>
		<ul style="list-style-type:none;">
			<li>Заказчик обязан оплатить Туристский продукт в соответствии с Договором.</li>
		</ul>
	</li>
</ul>
</body>
</html>