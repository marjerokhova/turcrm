<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ПРИХОДНЫЙ КАССОВЫЙ ОРДЕР</title>
  <style>
    @font-face {
      font-family: "DejaVu Sans";
      font-style: normal;
      src: url("/fonts/dejavu-sans/DejaVuSans.ttf");
      /* IE9 Compat Modes */
      src: 
        local("DejaVu Sans"), 
        local("DejaVu Sans"), 
        url("/fonts/dejavu-sans/DejaVuSans.ttf") format("truetype");
    }
    body { 
      font-family: "DejaVu Sans";
    }
  </style>
  <style type="text/css">
#sidebar{position:absolute;top:0;left:0;bottom:0;width:250px;padding:0;margin:0;overflow:auto}#page-container{position:absolute;top:0;left:0;margin:0;padding:0;border:0}@media screen{#sidebar.opened+#page-container{left:250px}#page-container{bottom:0;right:0;overflow:auto}.loading-indicator{display:none}.loading-indicator.active{display:block;position:absolute;width:64px;height:64px;top:50%;left:50%;margin-top:-32px;margin-left:-32px}.loading-indicator img{position:absolute;top:0;left:0;bottom:0;right:0}}@media print{@page{margin:0}html{margin:0}body{margin:0;-webkit-print-color-adjust:exact}#sidebar{display:none}#page-container{width:auto;height:auto;overflow:visible;background-color:transparent}.d{display:none}}.pf{position:relative;background-color:white;overflow:hidden;margin:0;border:0}.pc{position:absolute;border:0;padding:0;margin:0;top:0;left:0;width:100%;height:100%;overflow:hidden;display:block;transform-origin:0 0;-ms-transform-origin:0 0;-webkit-transform-origin:0 0}.pc.opened{display:block}.bf{position:absolute;border:0;margin:0;top:0;bottom:0;width:100%;height:100%;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;user-select:none}.bi{position:absolute;border:0;margin:0;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;user-select:none}@media print{.pf{margin:0;box-shadow:none;page-break-after:always;page-break-inside:avoid}@-moz-document url-prefix(){.pf{overflow:visible;border:1px solid #fff}.pc{overflow:visible}}}.c{position:absolute;border:0;padding:0;margin:0;overflow:hidden;display:block}.t{position:absolute;white-space:pre;font-size:1px;transform-origin:0 100%;-ms-transform-origin:0 100%;-webkit-transform-origin:0 100%;unicode-bidi:bidi-override;-moz-font-feature-settings:"liga" 0}.t:after{content:''}.t:before{content:'';display:inline-block}.t span{position:relative;unicode-bidi:bidi-override}._{display:inline-block;color:transparent;z-index:-1}::selection{background:rgba(127,255,255,0.4)}::-moz-selection{background:rgba(127,255,255,0.4)}.pi{display:none}.d{position:absolute;transform-origin:0 100%;-ms-transform-origin:0 100%;-webkit-transform-origin:0 100%}.it{border:0;background-color:rgba(255,255,255,0.0)}.ir:hover{cursor:pointer}</style>
<style type="text/css">
.m0{transform:matrix(0.250000,0.000000,0.000000,0.250000,0,0);-ms-transform:matrix(0.250000,0.000000,0.000000,0.250000,0,0);-webkit-transform:matrix(0.250000,0.000000,0.000000,0.250000,0,0);}
.v0{vertical-align:0.000000px;}
.ls0{letter-spacing:0.000000px;}
.sc_{text-shadow:none;}
.sc0{text-shadow:-0.015em 0 transparent,0 0.015em transparent,0.015em 0 transparent,0 -0.015em  transparent;}
@media screen and (-webkit-min-device-pixel-ratio:0){
.sc_{-webkit-text-stroke:0px transparent;}
.sc0{-webkit-text-stroke:0.015em transparent;text-shadow:none;}
}
.ws0{word-spacing:0.000000px;}
._1{width:13.539200px;}
._0{width:85.144000px;}
._2{width:103.208000px;}
._3{width:292.276000px;}
.fc0{color:rgb(0,0,0);}
.fs2{font-size:4;}
.fs0{font-size:5;}
.fs1{font-size:8;}
.fs3{font-size:7;}
.y0{bottom:275px;}
.y20{bottom:458.530000px;}
.y30{bottom:459.950000px;}
.y1f{bottom:470.690000px;}
.y2f{bottom:482.620000px;}
.y1e{bottom:486.880000px;}
.y2e{bottom:489.410000px;}
.y1d{bottom:499.030000px;}
.y2d{bottom:499.630000px;}
.y2c{bottom:522.310000px;}
.y2b{bottom:529.100000px;}
.y2a{bottom:548.640000px;}
.y1c{bottom:561.390000px;}
.y29{bottom:565.650000px;}
.y1b{bottom:575.560000px;}
.y1a{bottom:586.900000px;}
.y19{bottom:601.080000px;}
.y18{bottom:615.250000px;}
.y13{bottom:644.550000px;}
.y16{bottom:645.610000px;}
.yf{bottom:647.500000px;}
.y12{bottom:650.930000px;}
.y17{bottom:651.280000px;}
.y15{bottom:654.120000px;}
.y11{bottom:657.310000px;}
.yc{bottom:659.790000px;}
.y14{bottom:662.620000px;}
.y10{bottom:663.680000px;}
.y28{bottom:665.160000px;}
.ye{bottom:672.070000px;}
.yd{bottom:672.540000px;}
.y27{bottom:673.660000px;}
.y26{bottom:682.170000px;}
.ya{bottom:683.280000px;}
.yb{bottom:686.720000px;}
.y25{bottom:693.510000px;}
.y9{bottom:701.070000px;}
.y24{bottom:704.840000px;}
.y7{bottom:720.730000px;}
.y23{bottom:727.820000px;}
.y22{bottom:736.330000px;}
.y6{bottom:740.570000px;}
.y21{bottom:747.060000px;}
.y8{bottom:750.200000px;}
.y5{bottom:762.950000px;}
.y4{bottom:774.290000px;}
.y3{bottom:790.180000px;}
.y2{bottom:798.680000px;}
.y1{bottom:807.190000px;}
.h5{height:18.762500px;}
.h2{height:20.384766px;}
.h4{height:20.521484px;}
.h3{height:23.453125px;}
.h7{height:26.384766px;}
.h8{height:28.609375px;}
.h6{height:32.185547px;}
.h1{height:369.000000px;}
.h0{height:841.890000px;}
.w1{width:548.000000px;}
.w0{width:595.280000px;}
.x0{left:13.500000px;}
.x19{left:17.010000px;}
.xb{left:26.980000px;}
.x1a{left:73.700000px;}
.x12{left:83.900000px;}
.x7{left:88.780000px;}
.x9{left:86.620000px;}
.x11{left:99.360000px;}
.x6{left:114.230000px;}
.x1b{left:128.420000px;}
.x14{left:135.630000px;}
.xc{left:138.700000px;}
.x13{left:140.210000px;}
.x16{left:189.050000px;}
.x17{left:199.330000px;}
.x15{left:207.080000px;}
.x2{left:220.930000px;}
.x5{left:205.260000px;}
.x1{left:250.630000px;}
.x18{left:255.290000px;}
.xd{left:258.250000px;}
.x8{left:242.930000px;}
.x3{left:279.810000px;}
.x10{left:302.820000px;}
.xf{left:306.770000px;}
.xe{left:315.710000px;}
.xa{left:319.810000px;}
.x4{left:324.770000px;}
.x1f{left:394.020000px;}
.x1d{left:418.030000px;}
.x22{left:439.290000px;}
.x1e{left:442.980000px;}
.x1c{left:447.990000px;}
.x20{left:481.200000px;}
.x21{left:505.430000px;}
</style>
</head>
  <body>
	<div id="page-container">
	<div id="pf1" class="pf w0 h0" data-page-no="1"><div class="pc pc1 w0 h0"><img class="bi x0 y0 w1 h1" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABEgAAALiCAIAAABFTnQzAAAACXBIWXMAABYlAAAWJQFJUiTwAAAPEklEQVR42u3dwWobSxiE0anQ7//KlYXB3MUlicaS5m/NOQuTjRMog8afuxWn7QEA8F5JjuN46PuQE58C3McvEwAAAMIGAABA2AAAAAgbAABA2AAAAAgbAAAAYQMAACBsAAAAYQMAACBsAAAAhA0AAICwAQAAhA0AAICwAQAAEDYAAADCBgAAEDYAAADCBgAAQNgAAAAIGwAAQNgAAAAIGwAAAGEDAAAgbAAAAGEDAAAgbAAAAIQNAACAsAEAAIQNAACAsAEAABA2AAAAwgYAABA2AAAAwgYAAEDYAAAACBsAAEDYAAAACBsAAABhAwAAIGwAAABhAwAAIGwAAACEDQAAgLABAABuZJkAAPgYSYwA9+TEBgAA2J4TGwDgc7Q1AtyTExsAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAICwAQAAEDYAAADCBgAA4GHLBABMlsQIM7U1AjCHExsAAGB7TmwAmM7JwEBO0oBpnNgAAADCBgAAQNgAAAAIGwAAQNgAAAAIGwAAAGEDAAAgbAAAAGEDAAAgbAAAAIQNAACAsAEAAIQNAACAsAEAABA2AAAAwgYAABA2AAAAwgYAAEDYAAAAnLRMAMBYSb4/MvCr09YOwBBObAAAAGEDAABwNVfRANiAK0+juBwIDOTEBgAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAAAIGwAAAGEDAAAgbAAAAIQNAAAgbAAAAIQNAACAsAEAABA2AACAsAEAABA2AAAAwgYAAEDYAAAAd7VMAMB8SYwAgLABYG9tjaAzAf7AVTQAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAMAV1nEcSQwBMFnbmy/gUQXA38PG8xIAaYfOBLbmKhoAACBsAAAAhA0AAICwAQAAhA0AAICwAQAAEDYAAADCBgAAEDYAAADCBgAAQNgAAAAIGwAAQNgAAAAIGwAAAGEDAAAgbAAAAGEDAAAgbAAAAIQNAACAsAEAAIQNAACAsAEAABA2AAAAwgYAABA2AAAAwgYAAEDYAAAACBsAAEDYAAAACBsAAABhAwAAIGwAAABhAwAAIGwAAACEDQAAgLABAACEDQAAgLABAAB4r3UcRxJDAEzW1ggA8Jew8bwEAAC25ioaAAAgbAAAAIQNAACAsAEAAIQNAACAsAEAABA2AAAAwgYAABA2AAAAwgYAAEDYAAAACBsAAEDYAAAACBsAAIDrLBMAMF8SIwDwB05sAAAAYQMAAHA1V9EAmKutEQD4F05sAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAMC9+QWdALMkMQJb8OtTAWEDgO8X392KVlXgwGdzFQ0AABA2AAAAwgYAAEDYAAAAwgYAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAeNgyAcAcSb4/8optee6kbe0ADOHEBgAAEDYAAADCBgAA4Ie8xwZgIm9deKKvd9eY9OmTAozixAYAABA2AAAAwgYAAEDYAAAAwgYAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAFdZJgAYKIkRTArAv3NiAwAACBsAAICruYoGMEhbIwDACU5sAAAAYQMAACBsAAAAfmgd/gdMAOBx3hIGjAsbr03/9ZV5BgHOvYB49bDqTfbxU1FgGlfRAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAICwAQAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAAAIGwAAAGEDAAAgbAAAAIQNAABwV8sE/yuJEQAAQNjsre37U+r9/+ju8WkxGw6c1AgAcAlX0QAAAGEDAAAgbAAAAIQNAAAgbAAAAIQNAACAsAEAABA2AACAsAEAABA2AAAAwgYAAEDYAAAAwgYAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAICwAQAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAADcR0wAAJzT9vy3IMmjf8OJTwHuw4kNAAAgbAAAAIQNAADAD63vP7mx+uWq+7tJfAksZkOTYtUt9vl6VgKM4sQGAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAICwAQAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAADcU0wAAJzT9vy3IMmjf8OJTwHuw4kNAAAgbAAAAK62nOcCAAC7c2IDAAAIGwAAAGEDAAAgbAAAAGEDAAAgbAAAAIQNAACAsAEAAIQNAACAsAEAABA2AAAAwgYAABA2AAAAwgYAAEDYAAAACBsAAEDYAAAACBsAAABhAwAAIGwAAABhAwAAIGwAAACEDQAAgLABAACEDQAAgLABAAAQNgAAAMIGAAAQNgAAAMIGAABA2AAAADxsmeAVkhgBgLHaGgEQNnhgAADALK6iAQAAwgYAAEDYAAAACBsAAEDYAAAACBsAAABhAwAAIGwAAABhAwAAIGwAAACEDQAAgLABAACEDQAAgLABAAAQNgAAAMIGAAAQNgAAAMIGAABA2AAAAAgbAABA2AAAAAgbAAAAYQMAACBsAAAAYQMAACBsAAAArrBM8ApJjADAWG2NAAgbPDAAAGAWV9EAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAICwAQAAEDYAAABXWCZ4hSRGAGCstkYAhA0eGAAAMIuraAAAgLABAAAQNgAAAMIGAAAQNgAAAMIGAABA2AAAAAgbAABA2AAAAAgbAAAAYQMAACBsAAAAYQMAACBsAAAAhA0AAICwAQAAhA0AAICwAQAAEDYAAADCBgAAEDYAAACbWiZ4hSRGAGCstkYAhA0eGAAAMIuraAAAgLABAAAQNgAAAMIGAAAQNgAAAMIGAABA2AAAAAgbAABA2AAAAAgbAAAAYQMAACBsAAAAYQMAACBsAAAAhA0AAICwAQAAhA0AAICwAQAAEDYAAADCBgAAEDYAAADCBgAAQNgAAAAIGwAAQNgAAAAIGwAAgCssE7xHEiMAMERbIwAfxokNAACwPSc2b+JnYwAA8DpObAAAAGEDAAAgbAAAAIQNAAAgbAAAAIQNAACAsAEAABA2AACAsAEAABA2AAAAwgYAAEDYAAAAwgYAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAhA0AACBsAAAAhA0AAICwAQAAEDYAAICwAQAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAAAIGwAAAGEDAAAgbAAAAIQNAAAgbAAAAIQNAACAsAEAABA2AACAsAEAABA2AAAAwgYAAEDYAAAAd7BMAADAB0hiBGEDAAB7a2uEO3MVDQAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAAAIGwAAAGEDAAAgbAAAAIQNAAAgbAAAAIQNAACAsAEAABA2AACAsAEAABA2AAAAwgYAAEDYAAAAwgYAAEDYAAAACBsAAABhAwAACBsAAABhAwAAIGwAAACEDQAAIGwAAACEDQAAgLABAAAQNgAAgLABAAAQNgAAAMIGAABA2AAAAMIGAABA2AAAAAgbAAAAYQMAAAgbAAAAYQMAACBsAAAAHrBMAAB8jCRGAGEDALC3tkaAe3IVDQAAEDYAAADCBgAAQNgAAADCBgAAQNgAAAAIGwAAAGEDAAAIGwAAAGEDAAAgbAAAAIQNAAAgbAAAAIQNAACAsAEAABA2AACAsAEAABA2AAAAwgYAAEDYAAAAwgYAAGBTywQ8XRIjvEJbswNveD0BEDbgeWl2AIALuIoGAAAIGwAAAGEDAAAgbAAAAGEDAAAgbAAAAIQNAACAsAEAAIQNAADA1pYJAAD4AEmMIGwAAGBvbY1wZ66iAQAAwgYAAEDYAAAACBsAAEDYAAAACBsAAABhAwAAIGwAAABhAwAAIGwAAACEDQAAgLABAACEDQAAgLABAAAQNgAAAMIGAAAQNgAAAMIGAABA2AAAAAgbAABA2AAAAAgbAAAAYQMAACBsAAAAYQMAACBsAAAAhA0AAICwAQAAhA0AAICwAQAAEDYAAADCBgAAEDYAAADCBgAAQNgAAAAIGwAAQNgAAAAIGwAAAGEDAAAgbAAAAGEDAAAgbAAAAIQNAADAA5YJeLokRniFtmYH3vB64hkE7MiJDQAAsD0nNjzfB/8g0OwAXgyBmZzYAAAAwgYAAEDYAAAACBsAAEDYAAAACBsAAABhAwAAIGwAAABhAwAAIGwAAACEDQAAgLABAACEDQAAgLABAAAQNgAAAMIGAAAQNgAAAMIGAABA2AAAAAgbAABA2AAAAGzqNxot1QxHdvcgAAAAAElFTkSuQmCC"/><div class="t m0 x1 h2 y1 ff1 fs0 fc0 sc0 ls0 ws0">Унифицированная форма № КО-1</div><div class="t m0 x2 h2 y2 ff1 fs0 fc0 sc0 ls0 ws0">Утверждена постановлением Госкомстата</div><div class="t m0 x3 h2 y3 ff1 fs0 fc0 sc0 ls0 ws0">России от 18.08.98 г. №88</div><div class="t m0 x4 h3 y4 ff2 fs1 fc0 sc0 ls0 ws0">Код</div><div class="t m0 x5 h3 y5 ff2 fs1 fc0 sc0 ls0 ws0">Форма по ОКУД&nbsp;&nbsp;0310001</div><div class="t m0 x6 h4 y6 ff2 fs0 fc0 sc0 ls0 ws0">организация</div><div class="t m0 x7 h4 y7 ff2 fs0 fc0 sc0 ls0 ws0">структурное подразделение</div><div class="t m0 x8 h3 y8 ff2 fs1 fc0 sc0 ls0 ws0">по ОКПО</div><div class="t m0 x1 h5 y9 ff2 fs2 fc0 sc0 ls0 ws0">&nbsp;&nbsp;Номер документа&nbsp;&nbsp;&nbsp;&nbsp;Дата составления</div><div class="t m0 x9 h6 ya ff3 fs3 fc0 sc0 ls0 ws0">ПРИХОДНЫЙ КАССОВЫЙ ОРДЕР </div><div class="t m0 xa h4 yb ff2 fs0 fc0 sc0 ls0 ws0">{{ $params["date1"] }}</div><div class="t m0 xb h4 yc ff2 fs0 fc0 sc0 ls0 ws0">Дебет</div><div class="t m0 xc h4 yd ff2 fs0 fc0 sc0 ls0 ws0">Кредит</div><div class="t m0 xd h4 yc ff2 fs0 fc0 sc0 ls0 ws0">Сумма</div><div class="t m0 xe h4 ye ff2 fs0 fc0 sc0 ls0 ws0">Код</div><div class="t m0 xf h4 yc ff2 fs0 fc0 sc0 ls0 ws0">целевого</div><div class="t m0 x10 h4 yf ff2 fs0 fc0 sc0 ls0 ws0">назначения</div><div class="t m0 x11 h4 y10 ff2 fs0 fc0 sc0 ls0 ws0">код</div><div class="t m0 x12 h4 y11 ff2 fs0 fc0 sc0 ls0 ws0">структурного</div><div class="t m0 x12 h4 y12 ff2 fs0 fc0 sc0 ls0 ws0">подразделе-</div><div class="t m0 x11 h4 y13 ff2 fs0 fc0 sc0 ls0 ws0">ния</div><div class="t m0 xc h4 y14 ff2 fs0 fc0 sc0 ls0 ws0">корреспон-</div><div class="t m0 x13 h4 y15 ff2 fs0 fc0 sc0 ls0 ws0">дирующий</div><div class="t m0 x14 h4 y16 ff2 fs0 fc0 sc0 ls0 ws0">счет, субсчет</div><div class="t m0 x15 h4 y14 ff2 fs0 fc0 sc0 ls0 ws0">код</div><div class="t m0 x16 h4 y15 ff2 fs0 fc0 sc0 ls0 ws0">аналитическо-</div><div class="t m0 x17 h4 y16 ff2 fs0 fc0 sc0 ls0 ws0">го учета</div><div class="t m0 x18 h4 y17 ff2 fs0 fc0 sc0 ls0 ws0">руб. коп.</div><div class="t m0 x19 h7 y18 ff2 fs3 fc0 sc0 ls0 ws0">Принято от:{{ $params["name"] }}</div><div class="t m0 x19 h7 y19 ff2 fs3 fc0 sc0 ls0 ws0">Основание: {{ $params["purpose"] }}</div><div class="t m0 x19 h7 y1a ff2 fs3 fc0 sc0 ls0 ws0">Сумма:<span class="_ _2"> </span>{{ $params["price_text"] }}</div><div class="t m0 x1a h7 y1b ff2 fs3 fc0 sc0 ls0 ws0">без налога (НДС)</div><div class="t m0 x19 h7 y1c ff2 fs3 fc0 sc0 ls0 ws0">Приложение:</div><div class="t m0 x19 h6 y1d ff3 fs3 fc0 sc0 ls0 ws0">Главный бухгалтер</div><div class="t m0 x1b h4 y1e ff2 fs0 fc0 sc0 ls0 ws0">подпись&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;расшифровка подписи</div><div class="t m0 x19 h6 y1f ff3 fs3 fc0 sc0 ls0 ws0">Получил кассир</div><div class="t m0 x1b h4 y20 ff2 fs0 fc0 sc0 ls0 ws0">подпись&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;расшифровка подписи</div><div class="t m0 x1c h6 y21 ff3 fs3 fc0 sc0 ls0 ws0">КВИТАНЦИЯ</div><div class="t m0 x1d h4 y22 ff2 fs0 fc0 sc0 ls0 ws0">к приходному кассовому ордеру № </div><div class="t m0 x1e h4 y23 ff2 fs0 fc0 sc0 ls0 ws0">от {{ $params["date2"] }}</div><div class="t m0 x1f h3 y24 ff2 fs1 fc0 sc0 ls0 ws0">Принято от: {{ $params["name"] }}</div><div class="t m0 x1f h3 y25 ff2 fs1 fc0 sc0 ls0 ws0">Основание: {{ $params["purpose"] }}</div><div class="t m0 x1f h3 y26 ff2 fs1 fc0 sc0 ls0 ws0">Сумма: </div><div class="t m0 x1f h3 y27 ff2 fs1 fc0 sc0 ls0 ws0">{{ $params["price_text"] }}</div><div class="t m0 x1f h3 y28 ff2 fs1 fc0 sc0 ls0 ws0">без налога (НДС)</div><div class="t m0 x20 h6 y29 ff3 fs3 fc0 sc0 ls0 ws0">{{ $params["date2"] }}</div><div class="t m0 x1f h6 y2a ff3 fs3 fc0 sc0 ls0 ws0">М.П.(штампа)</div><div class="t m0 x1f h8 y2b ff3 fs1 fc0 sc0 ls0 ws0">Главный бухгалтер </div><div class="t m0 x21 h4 y2c ff2 fs0 fc0 sc0 ls0 ws0">подпись</div><div class="t m0 x22 h4 y2d ff2 fs0 fc0 sc0 ls0 ws0"> расшифровка подписи</div><div class="t m0 x1f h8 y2e ff3 fs1 fc0 sc0 ls0 ws0">Кассир </div><div class="t m0 x21 h4 y2f ff2 fs0 fc0 sc0 ls0 ws0">подпись</div><div class="t m0 x22 h4 y30 ff2 fs0 fc0 sc0 ls0 ws0">расшифровка подписи</div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
	</div>
  </body>
</html>
