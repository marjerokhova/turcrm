@extends('layouts.app')

@section('content')

<!-- Bootstrap шаблон... -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">	
			<div class="panel-body m-2">
				<!-- Отображение ошибок проверки ввода -->
				@include('common.errors')
				<button type="button" class="btn btn-info mb-2" data-toggle="collapse" data-target="#new_tour">Добавить новый тур</button>
				<div id="new_tour" class="collapse">
					<!-- Форма нового тура -->
					<form  id="save_tour_form"  action="{{ url('tours') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field1" class="control-label">Название тура</label>
									<div>
										<input type="text" name="field1" id="tours-field1" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field2" class="control-label">Имя клиента</label>
									<div>
										<input type="text" name="field2" id="tours-field2" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field3" class="control-label">Поле 3</label>
									<div>
										<input type="text" name="field3" id="tours-field3" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field4" class="control-label">Поле 4</label>
									<div>
										<input type="text" name="field4" id="tours-field4" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field5" class="control-label">Поле 5</label>
									<div>
										<input type="text" name="field5" id="tours-field5" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field6" class="control-label">Поле 6</label>
									<div>
										<input type="text" name="field6" id="tours-field6" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field7" class="control-label">Поле 7</label>
									<div>
										<input type="text" name="field7" id="tours-field7" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field8" class="control-label">Поле 8</label>
									<div>
										<input type="text" name="field8" id="tours-field8" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field9" class="control-label">Поле 9</label>
									<div>
										<input type="text" name="field9" id="tours-field9" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field10" class="control-label">Поле 10</label>
									<div>
										<input type="text" name="field10" id="tours-field10" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field11" class="control-label">Поле 11</label>
									<div>
										<input type="text" name="field11" id="tours-field11" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field12" class="control-label">Поле 12</label>
									<div>
										<input type="text" name="field12" id="tours-field12" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field13" class="control-label">Поле 13</label>
									<div>
										<input type="text" name="field13" id="tours-field13" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field14" class="control-label">Поле 14</label>
									<div>
										<input type="text" name="field14" id="tours-field14" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field15" class="control-label">Поле 15</label>
									<div>
										<input type="text" name="field15" id="tours-field15" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field16" class="control-label">Поле 16</label>
									<div>
										<input type="text" name="field16" id="tours-field16" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field17" class="control-label">Поле 17</label>
									<div>
										<input type="text" name="field17" id="tours-field17" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field18" class="control-label">Поле 18</label>
									<div>
										<input type="text" name="field18" id="tours-field18" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field19" class="control-label">Поле 19</label>
									<div>
										<input type="text" name="field19" id="tours-field19" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field20" class="control-label">Поле 20</label>
									<div>
										<input type="text" name="field20" id="tours-field20" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field21" class="control-label">Поле 21</label>
									<div>
										<input type="text" name="field21" id="tours-field21" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field22" class="control-label">Поле 22</label>
									<div>
										<input type="text" name="field22" id="tours-field22" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field23" class="control-label">Поле 23</label>
									<div>
										<input type="text" name="field23" id="tours-field23" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field24" class="control-label">Поле 24</label>
									<div>
										<input type="text" name="field24" id="tours-field24" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field25" class="control-label">Поле 25</label>
									<div>
										<input type="text" name="field25" id="tours-field25" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field26" class="control-label">Поле 26</label>
									<div>
										<input type="text" name="field26" id="tours-field26" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field27" class="control-label">Поле 27</label>
									<div>
										<input type="text" name="field27" id="tours-field27" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field28" class="control-label">Поле 28</label>
									<div>
										<input type="text" name="field28" id="tours-field28" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field29" class="control-label">Поле 29</label>
									<div>
										<input type="text" name="field29" id="tours-field29" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field30" class="control-label">Поле 30</label>
									<div>
										<input type="text" name="field30" id="tours-field30" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field31" class="control-label">Поле 31</label>
									<div>
										<input type="text" name="field31" id="tours-field31" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field32" class="control-label">Поле 32</label>
									<div>
										<input type="text" name="field32" id="tours-field32" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field33" class="control-label">Поле 33</label>
									<div>
										<input type="text" name="field33" id="tours-field33" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field34" class="control-label">Поле 34</label>
									<div>
										<input type="text" name="field34" id="tours-field34" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field35" class="control-label">Поле 35</label>
									<div>
										<input type="text" name="field35" id="tours-field35" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<div class="form-group">
									<label for="field36" class="control-label">Поле 36</label>
									<div>
										<input type="text" name="field36" id="tours-field36" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field37" class="control-label">Поле 37</label>
									<div>
										<input type="text" name="field37" id="tours-field37" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field38" class="control-label">Поле 38</label>
									<div>
										<input type="text" name="field38" id="tours-field38" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field39" class="control-label">Поле 39</label>
									<div>
										<input type="text" name="field39" id="tours-field39" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="field40" class="control-label">Поле 40</label>
									<div>
										<input type="text" name="field40" id="tours-field40" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row bg-info">
							<div class="col-sm">
								<div class="form-group">
									<label for="cost_price" class="control-label">Себестоимость</label>
									<div>
										<input type="number" name="cost_price" id="tours-cost-price" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="cost" class="control-label">Цена</label>
									<div>
										<input type="number" name="cost" id="tours-cost" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="comission" class="control-label">Комиссия</label>
									<div>
										<input type="number" name="comission" id="tours-comission" class="form-control" readonly>
									</div>
								</div>
							</div>
						</div>
						<div class="row bg-info">
							<div class="col-sm">
								<div class="form-group">
									<label for="incoming" class="control-label">Приход</label>
									<div>
										<input type="number" name="incoming" id="tours-incoming" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-sm">
								<div class="form-group">
									<label for="outgoing" class="control-label">Расход</label>
									<div>
										<input type="number" name="outgoing" id="tours-outgoing" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<!-- Кнопка добавления тура -->
						<div class="form-group">
							<div class="my-2">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-plus"></i> Сохранить
								</button>
							</div>
						</div>
					</form>
					<div class="row bg-success d-none tours-debt-info m-2">
						<div class="col-sm">
							<div class="form-group">
								<label for="debt1" class="control-label">Долг перед туроператором</label>
								<div>
									<input type="number" name="debt1" class="tours-debt1 form-control" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label for="debt2" class="control-label">Долг перед турагентом</label>
								<div>
									<input type="number" name="debt2" class="tours-debt2 form-control" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label for="prize" class="control-label">Премия</label>
								<div>
									<input type="text" name="prize" class="tours-prize form-control" readonly>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Текущие туры -->
			@if (count($tours) > 0)
			<div class="panel panel-default m-2">
				<div class="panel-heading">
					Все туры
				</div>

				<div class="panel-body">
					<table class="table table-striped task-table">
						<!-- Заголовок таблицы -->
						<!--<thead>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</thead>-->

						<!-- Тело таблицы -->
						<tbody>
							@foreach ($tours as $tour)
							<tr>
								<!-- Имя задачи -->
								<td class="table-text">
									<div>{{ $tour->field1 }}</div>
								</td>

								<!-- Кнопка Удалить -->
								<td>
									<form action="{{ url('tours/'.$tour->id) }}" method="POST">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}

										<button type="submit" class="btn btn-danger">
											<i class="fa fa-trash"></i> Удалить
										</button>
									</form>
								</td>
								<!-- Кнопка Удалить -->
								<td>
									<form action="{{ url('tours/'.$tour->id) }}" method="GET">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-danger">
											<i class="fa fa-trash"></i> Редактировать
										</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
